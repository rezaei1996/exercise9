package ir.coffeecode.animations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class actMain extends AppCompatActivity implements View.OnClickListener {

    int temp = 0;
    SeekBar sek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_main);

        Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(actMain.this);

        Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(actMain.this);

        Button btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(actMain.this);

        Button btn4 = (Button) findViewById(R.id.btn4);
        btn4.setOnClickListener(actMain.this);

        Button btn5 = (Button) findViewById(R.id.btn5);
        btn5.setOnClickListener(actMain.this);

        Button btn6 = (Button) findViewById(R.id.btn6);
        btn6.setOnClickListener(actMain.this);

        final TextView txv = (TextView) findViewById(R.id.txv);

        sek = (SeekBar) findViewById(R.id.sek);

        sek.setMax(1000);
        sek.setProgress(500);
        sek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txv.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        temp = sek.getProgress();
        switch (view.getId()) {
            case R.id.btn1:
                YoYo.with(Techniques.Tada)
                        .duration(temp)
                        .playOn(findViewById(R.id.img));
                break;
            case R.id.btn2:
                YoYo.with(Techniques.Bounce)
                        .duration(temp)
                        .playOn(findViewById(R.id.img));
                break;
            case R.id.btn3:
                YoYo.with(Techniques.FadeIn)
                        .duration(temp)
                        .playOn(findViewById(R.id.img));
                break;
            case R.id.btn4:
                YoYo.with(Techniques.Flash)
                        .duration(temp)
                        .playOn(findViewById(R.id.img));
                break;
            case R.id.btn5:
                YoYo.with(Techniques.ZoomIn)
                        .duration(temp)
                        .playOn(findViewById(R.id.img));
                break;
            case R.id.btn6:
                YoYo.with(Techniques.Wave)
                        .duration(temp)
                        .playOn(findViewById(R.id.img));
                break;

        }
    }
}
